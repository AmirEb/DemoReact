import {REGISTER_USER,} from '../types';

import axios from 'axios';
import {SIGNUP} from '../../utils/misc';


export function SignUp(data){

    const request = axios({
        method:"POST",
        url:SIGNUP,
        data:{
            email: data.email,
            password: data.password,
            returnSecureToken:true
        },
        headers:{
            "Content-Type":"application/json"
        }
    }).then( response => {
        return response.data
    }).catch(e =>{
        return false
    });

    return {
        type: REGISTER_USER,
        payload: request
    }
}


