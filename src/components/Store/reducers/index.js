import { combineReducers } from 'redux';
import authReducer from './user_reducer'
import projectReducer from './articles_reducer';

const rootReducer = combineReducers({
    auht:authReducer,
    project:projectReducer
});

export default rootReducer;
