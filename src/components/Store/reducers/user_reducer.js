import {
    REGISTER_USER,
} from '../types';

const initState = {};

const authReducer =(state = initState,action)=>{
    switch(action.type){

        case REGISTER_USER:
            return {
                ...state,
                userData:{
                    uid:action.payload.localId || false,
                    token:action.payload.idToken || false,
                    refToken:action.payload.refreshToken || false
                }
            };
            break;

        default:
            return state
    }

};

export default authReducer;
