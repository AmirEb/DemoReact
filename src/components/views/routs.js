import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import DetailsScreen from './Admin/Admin'
import LoginForm from './Login/loginForm';
const Navigator = createSwitchNavigator(
	{
		LoginPanel: {
			screen: LoginForm,
		},
		Dashboard: {
			screen: DetailsScreen,
		},
	},{headerMode:'none',
		mode:'modal'
	}
);

const AppContainer = createAppContainer(Navigator);

export {AppContainer}



