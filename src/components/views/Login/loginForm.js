import React , {Component} from 'react';
import { Container, Input, Content, Form, Item ,Text} from 'native-base';
import {StyleSheet,View,Button} from 'react-native';
import validationRules from '../../utils/forms/validationRules'
import {connect} from 'react-redux'
import {SignUp} from '../../Store/actions/user_actions';
import {bindActionCreators} from 'redux'


class LoginForm extends Component {

	state ={
		checker:'',
		type:'Login',
		action:'ورود',
		actionMode:'میخواهم ثبت نام کنم',
		hasErrors:false,
		form:{
			email:{
				value:'',
				valid:false,
				type:'textInput',
				rules:{
					isRequired:true,
					isEmail:true
				}
			},
			password:{
				value:'',
				valid:false,
				type:'textinput',
				rules:{
					isRequired:true,
					minLength:6
				}
			},
			confirmPassword:{
				value:'',
				valid:false,
				type:'textinput',
				rules:{
					confirmPass:"password"
				}
			}
		}
	};



	inputHandler =(name,value) =>{
		this.setState({
			hasErrors:false
		});

		let formCopy = this.state.form;
		formCopy[name].value = value;

		let rules = formCopy[name].rules;
		let valid = validationRules(value,rules,formCopy);

		formCopy[name].valid = valid;


		this.setState({
			form:formCopy
		})
	};




	confirmPassword = ()=>(
		this.state.type !=='Login' ?
			<Item>
				<Input
					placeholder="تکرار پسورد"
					type={this.state.form.confirmPassword.type}
					value={this.state.form.confirmPassword.value}
					onChangeText={value =>this.inputHandler("confirmPassword",value)}
					secureTextEntry={true}
				/>
			</Item>
			:null
	);

	submitUserHandler = ()=>{
		let isFromValid = true;
		let formToSubmit= {};
		const formCopy = this.state.form;

		for(let key in formCopy){
			if(this.state.type==='Login'){
				if(key !== 'confirmPassword'){
					isFromValid = isFromValid && formCopy[key].valid;
					formToSubmit[key] = formCopy[key].value
				}
			}
			else {
				isFromValid = isFromValid && formCopy[key].valid;
				formToSubmit[key] = formCopy[key].value
			}
		}
		if(isFromValid){
			if(this.state.type==="Login"){
				this.props.SignUp(formToSubmit).then(()=>{

				})
			}
			else{
				this.props.SignUp(formToSubmit).then(()=>{
					this.setState({
						checker:'کار موکوند'
					})
				})
			}
		}
		else {
			this.setState({
				hasErrors:true
			})
		}
	};



	formErrorsHandler = ()=>(
		this.state.hasErrors ?
			<View style={styles.errorContainer}>
			<Text style={styles.errorLabel}>ورودی های خود را بررسی کنید</Text>
			</View>

			:null

	);


	changeFormType = ()=>{
		const type = this.state.type;
		this.setState({
			type: type==='Login'? 'Register' : 'Login',
			action: type==='Login'? 'ثبت نام' : 'ورود',
			actionMode: type === 'Login'? 'قبلا ثبت نام کرده ام' : 'میخواهم ثبت نام کنم',
			hasErrors:false,
		})
	};



	render(){
		return (

			<Container style={styles.formInputContainer}>

				<Content>
					<Form>
						<Item>
							<Input placeholder="ایمیل خود را وارد کنید"
								   type={this.state.form.email.type}
								   value={this.state.form.email.value}
								   onChangeText={value =>this.inputHandler("email",value)}
								   autoCapitalize={"none"}
								   keyboardType={"email-address"}
								   style={styles.input}
							/>
						</Item>
						<Item >
							<Input placeholder="پسورد وارد کنید"
								   type={this.state.form.password.type}
								   value={this.state.form.password.value}
								   onChangeText={value =>this.inputHandler("password",value)}
								   secureTextEntry={true}
								   style={styles.input}
								   autoCapitalize={"none"}
							/>
						</Item>


						{this.confirmPassword()}
						{this.formErrorsHandler()}
						<Text>{this.state.checker}</Text>


							<Button title={"avali"}
								onPress={this.submitUserHandler}
								style={styles.loginButton}
							 >
								<Text> {this.state.action} </Text>
							</Button>

						<Button title={"avalid"}
							onPress={this.changeFormType}
							style={styles.buttonStyle}
						>
							<Text> {this.state.actionMode} </Text>
						</Button>


							<Button title={"avalis"}
									style={styles.buttonStyle}
									onPress={() => this.props.navigation.navigate('Dashboard')}

							>
								<Text> بعدا انجامش می دهنم </Text>
							</Button>

					</Form>
				</Content>
			</Container>

		)
	}
}

const styles = StyleSheet.create({
	input:{
		width:"100%",
		borderBottomWidth:2,
		borderBottomColor:"#eaeaea",
		fontSize:18,
		padding:5,
		marginTop:15
	},
	formInputContainer:{
		minHeight:400,
		backgroundColor:"#ababab"
	},

	buttonStyle:{
		marginBottom:10,
		marginTop: 10,
		alignSelf:'center'
	},
	loginButton:{
		marginBottom:10,
		marginTop: 10
	},
	errorContainer:{
		marginBottom:20,
		marginTop:10
	},
	errorLabel:{
		color:'red',
		fontFamily:'Roboto-Black'
	}

});




const ConnectedRoot = connect(
	(state) => ({
		state: state.user
	}),
	(dispatch) => ({
		actions: bindActionCreators(SignUp, dispatch)
	})
)(LoginForm);

export default ConnectedRoot

