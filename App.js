import React, { Component } from 'react';
import {AppContainer} from './src/components/views/routs';
import {Provider} from 'react-redux'
import rootReducer from './src/components/Store/reducers/index'
import {createStore} from 'redux';
import {StyleSheet,Text} from 'react-native';



const store = createStore(rootReducer);

class App extends Component {
    render() {

        return <Provider store={store}><AppContainer/></Provider>
    }
}
export default App;





